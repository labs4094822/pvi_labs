const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { studentsService } = require("./services/students.service");
const { validateUserData } = require("./utils/validateStudentData");

const app = express();
const port = 8000;

// CORS middleware
app.use(cors());

// Body parser middleware
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.post("/student/add", async (req, res) => {
  try {
    const error = validateUserData(req.body);
    if (error) {
      res.send({
        success: false,
        message: error,
      });
      return;
    }

    const data = await studentsService.addStudent(req.body);

    res.send({
      success: true,
      data,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error adding student",
    });
  }
});

app.post("/student/edit/:id", async (req, res) => {
  try {
    const error = validateUserData(req.body);
    if (error) {
      res.send({
        success: false,
        message: error,
      });
      return;
    }

    const data = await studentsService.editUserById(req.params.id, req.body);

    res.send({
      success: true,
      data,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error editing student",
    });
  }
});

app.delete("/student/delete/:id", async (req, res) => {
  try {
    const studentId = await studentsService.deleteStudent(req.params.id);

    res.send({
      success: true,
      data: studentId,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error deleting student with this id",
    });
  }
});

app.get("/student/all", async (req, res) => {
  try {
    const allStudents = await studentsService.getAllStudents();

    res.send({
      success: true,
      data: allStudents,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Can't get all students",
    });
  }
});

app.get("/student/:id", async (req, res) => {
  try {
    const studentData = await studentsService.getUserById(req.params.id);

    res.send({
      success: true,
      data: studentData,
    });
  } catch (error) {
    res.send({
      success: false,
      message: "Error getting user with this id",
    });
  }
});

app.post('/register', async (req, res) => {
  const { name, email, password } = req.body;
  try {
      const { data: existingUser, error: fetchError } = await supabase
          .from('users')
          .select('id')
          .eq('email', email)
          .single();

      if (existingUser) {
          return res.status(400).send('Email already exists');
      }

      const hashedPassword = await bcrypt.hash(password, 10);
      const { data, error } = await supabase
          .from('users')
          .insert([{ name, email, password: hashedPassword }])
          .single();

      if (error) throw error;

      req.session.userId = data.id;
      res.status(201).send('User registered successfully');
  } catch (error) {
      res.status(500).send('Error registering user');
  }
});

// Login endpoint
app.post('/login', async (req, res) => {
  const { email, password } = req.body;
  try {
      const { data: user, error } = await supabase
          .from('users')
          .select('*')
          .eq('email', email)
          .single();

      if (error || !user) {
          return res.status(400).send('Invalid email or password');
      }

      const match = await bcrypt.compare(password, user.password);
      if (!match) {
          return res.status(400).send('Invalid email or password');
      }

      req.session.userId = user.id;
      res.send({
        message: 'Logged in successfully',
        userName: user.name
      });
  } catch (error) {
      res.status(500).send('Error logging in');
  }
});

// Middleware to check if the user is authenticated
function isAuthenticated(req, res, next) {
  if (req.session.userId) {
      return next();
  } else {
      res.status(401).send('You need to log in first');
  }
}


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
