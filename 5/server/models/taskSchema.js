const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema({
    content: {type: String, required: true},
    column: {type: String, required: true},
});

module.exports.TaskModel = mongoose.model('Tasks',TaskSchema);
module.exports.TaskSchema = TaskSchema;