const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const { studentsService } = require("./services/students.service");
const { validateUserData } = require("./utils/validateStudentData");
const mongoose = require("mongoose");
const authRoute = require("./routes/auth");
const chatRoute = require("./routes/chat");
const ChatRoom = require("./models/chatRoomSchema");
const http = require("http");
const { Server } = require("socket.io");
const session = require("express-session");
const TaskSchema = require("./models/taskSchema");
const Tasks = TaskSchema.TaskModel;

const app = express();

// CORS middleware
app.use(cors());

// Body parser middleware
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());

app.use(
    session({
        secret: "your_secret_key", // Replace with a secret key of your choice
        resave: false,
        saveUninitialized: true,
        cookie: { secure: false }, // Set secure to true if using https
    })
);

app.post("/student/add", async (req, res) => {
    try {
        const error = validateUserData(req.body);
        if (error) {
            res.send({
                success: false,
                message: error,
            });
            return;
        }

        const data = await studentsService.addStudent(req.body);

        res.send({
            success: true,
            data,
        });
    } catch (error) {
        res.send({
            success: false,
            message: "Error adding student",
        });
    }
});

app.post("/student/edit/:id", async (req, res) => {
    try {
        const error = validateUserData(req.body);
        if (error) {
            res.send({
                success: false,
                message: error,
            });
            return;
        }

        const data = await studentsService.editUserById(
            req.params.id,
            req.body
        );

        res.send({
            success: true,
            data,
        });
    } catch (error) {
        res.send({
            success: false,
            message: "Error editing student",
        });
    }
});

app.delete("/student/delete/:id", async (req, res) => {
    try {
        const studentId = await studentsService.deleteStudent(req.params.id);

        res.send({
            success: true,
            data: studentId,
        });
    } catch (error) {
        res.send({
            success: false,
            message: "Error deleting student with this id",
        });
    }
});

app.get("/student/all", async (req, res) => {
    try {
        const allStudents = await studentsService.getAllStudents();

        res.send({
            success: true,
            data: allStudents,
        });
    } catch (error) {
        res.send({
            success: false,
            message: "Can't get all students",
        });
    }
});

app.get("/student/:id", async (req, res) => {
    try {
        const studentData = await studentsService.getUserById(req.params.id);

        res.send({
            success: true,
            data: studentData,
        });
    } catch (error) {
        res.send({
            success: false,
            message: "Error getting user with this id",
        });
    }
});

app.get("/tasks", async (req, res) => {
    Tasks.find().then((data) => {
        res.send({
            success: true,
            tasks: data,
        });
    });
});

// Columns: todo, inProgress, qaReady, done
app.post("/tasks", async (req, res) => {
    const task = new Tasks({ content: req.body.content, column: "todo" });
    task.save();

    Tasks.find().then((data) => {
        res.send({
            success: true,
            tasks: task,
        });
    });
});

app.patch("/tasks/:taskId", async (req, res) => {
    const filter = { _id: req.params.taskId }; // Replace with your filter
    const update = {
        column: req.body.column, // Replace with the field and value you want to update
    };

    Tasks.findOneAndUpdate(filter, update, { new: true }).then((data) => {
        Tasks.find().then(data2 => {
            res.send({
                success: true,
                tasks: data2
            });
        })
    
    });
});

// Middleware to check if the user is authenticated
function isAuthenticated(req, res, next) {
    if (req.session.userId) {
        return next();
    } else {
        res.status(401).send("You need to log in first");
    }
}

mongoose
    .connect(
        "mongodb+srv://vovastepanov741:vova2005@cluster0.jnc5uer.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0",
        { useNewUrlParser: true, useUnifiedTopology: true }
    )
    .then(() => {
        console.log("Connected to MongoDB");
    })
    .catch((error) => {
        console.error("Error connecting to MongoDB:", error);
    });

app.use("/auth", authRoute);
app.use("/chat", chatRoute);

const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
    },
});

io.on("connection", (socket) => {
    socket.on("join_room", (data) => {
        socket.join(data);
    });
    socket.on("send_message", (data) => {
        socket.in(data.room).emit("recieve_message", data);
        ChatRoom.updateOne(
            { _id: data.room },
            {
                $push: {
                    messages: {
                        author: data.author,
                        message: data.message,
                        time: data.time,
                        authorId: data.authorId,
                    },
                },
            }
        ).then((res) => {});
    });
    socket.on("disconnect", () => {});
});

server.listen(3001, () => {
    console.log("SERVER RUNNING");
});
