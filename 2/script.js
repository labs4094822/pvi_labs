class StudentsApp {
  _defaultStudents = JSON.parse(localStorage.getItem("users") ?? "[]");

  _activeStudentId = null;
  _formView = null;
  _tableBody = document.querySelector(".students-table tbody");

  run = () => {
    this._initModals();

    this._initTable();
    this._initSelectAllCheckbox();
  };

  _showToast = (text, type = "success") => {
    Toastify({
      text,
      duration: 5000,
      close: true,
      gravity: "top",
      position: "right",
      ...(type === "error" && {
        style: {
          background: "red",
        },
      }),
    }).showToast();
  };

  _validateDate = (inputDate) => {
    const date = new Date(inputDate);
    const currentDate = new Date();

    const maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() - 100);

    if (date > currentDate || date < maxDate) {
      return false;
    }

    return true;
  };

  _objectToUrlParams = (obj) => {
    return Object.keys(obj)
      .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`)
      .join("&");
  };

  _initModals = () => {
    const modals = document.querySelectorAll(".modal");

    modals.forEach((modal) => {
      const closeButtons = modal.querySelectorAll(".close-button");
      const overlay = modal.querySelector(".overlay");

      overlay.addEventListener("click", () => modal.classList.remove("active"));
      closeButtons.forEach((button) => {
        button.addEventListener("click", () => modal.classList.remove("active"));
      });
    });

    this._initAddStudentModal();
    this._initEditStudentModal();
    this._initDeleteStudentModal();
  };

  _initTable = () => {
    this._tableBody.innerHTML = "";
    this._defaultStudents.forEach((student) => {
      this._createTableRow(student);
    });
  };

  _initAddStudentModal = () => {
    const addUserModal = document.querySelector(".edit-user-modal");
    const addStudentButton = document.querySelector(".add-user-button");

    addStudentButton.addEventListener("click", () => this._openAddUserModal());

    const addUserForm = addUserModal.querySelector("#edit-student-form");

    const groupInput = addUserModal.querySelector("#group");
    const firstNameInput = addUserModal.querySelector("#first-name");
    const lastNameInput = addUserModal.querySelector("#last-name");
    const genderInput = addUserModal.querySelector("#gender");
    const birthdayInput = addUserModal.querySelector("#birthday");

    addUserForm.addEventListener("submit", (e) => {
      e.preventDefault();
      if (this._formView === "add") {
        const newId =
          this._defaultStudents.length > 0
            ? this._defaultStudents[this._defaultStudents.length - 1].id + 1
            : 0;
        const newGroup = groupInput.value;
        const newFirstName = firstNameInput.value;
        const newLastName = lastNameInput.value;
        const newGender = genderInput.value;
        const newBirthday = birthdayInput.value;

        if (!newGroup) {
          this._showToast("Select group", "error");
          return;
        }
        if (!newFirstName) {
          this._showToast("Enter first name", "error");
          return;
        }
        if (!newLastName) {
          this._showToast("Enter last name", "error");
          return;
        }
        if (!newGender) {
          this._showToast("Select gender", "error");
          return;
        }
        if (!newBirthday) {
          this._showToast("Enter birthday", "error");
          return;
        }
        if (!this._validateDate(newBirthday)) {
          this._showToast("Birthday is invalid", "error");
          return;
        }

        this._defaultStudents.push({
          id: newId,
          group: newGroup,
          firstName: newFirstName,
          lastName: newLastName,
          gender: newGender,
          birthday: newBirthday,
          isActive: false,
        });

        localStorage.setItem("users", JSON.stringify(this._defaultStudents));

        // request
        const reqObj = {
          id: this._activeStudentId,
          firstName: newFirstName,
          lastName: newLastName,
          gender: newGender,
          group: newGroup,
          birthday: newBirthday,
        };
        console.log(reqObj);
        // fetch(`http://some-url.net?${this._objectToUrlParams(reqObj)}`, {
        //   method: "GET",
        // });

        this._initTable();

        this._showToast("Success!");
      }
    });
  };

  _initEditStudentModal = () => {
    const editModal = document.querySelector(".edit-user-modal");
    const addButton = editModal.querySelector(".save-button");

    const groupInput = editModal.querySelector("#group");
    const firstNameInput = editModal.querySelector("#first-name");
    const lastNameInput = editModal.querySelector("#last-name");
    const genderInput = editModal.querySelector("#gender");
    const birthdayInput = editModal.querySelector("#birthday");

    addButton.addEventListener("click", () => {
      if (this._formView === "edit") {
        const newGroup = groupInput.value;
        const newFirstName = firstNameInput.value;
        const newLastName = lastNameInput.value;
        const newGender = genderInput.value;
        const newBirthday = birthdayInput.value;

        if (!newGroup) {
          this._showToast("Select group", "error");
          return;
        }
        if (!newFirstName) {
          this._showToast("Enter first name", "error");
          return;
        }
        if (!newLastName) {
          this._showToast("Enter last name", "error");
          return;
        }
        if (!newGender) {
          this._showToast("Select gender", "error");
          return;
        }
        if (!newBirthday) {
          this._showToast("Enter birthday", "error");
          return;
        }
        if (!this._validateDate(newBirthday)) {
          this._showToast("Birthday is invalid", "error");
          return;
        }

        this._defaultStudents.forEach((student, index) => {
          if (student.id === this._activeStudentId) {
            this._defaultStudents[index]["group"] = newGroup;
            this._defaultStudents[index]["firstName"] = newFirstName;
            this._defaultStudents[index]["lastName"] = newLastName;
            this._defaultStudents[index]["gender"] = newGender;
            this._defaultStudents[index]["birthday"] = newBirthday;
          }
        });

        // request
        const reqObj = {
          id: this._activeStudentId,
          firstName: newFirstName,
          lastName: newLastName,
          gender: newGender,
          group: newGroup,
          birthday: newBirthday,
        };
        console.log(reqObj);
        // fetch(`http://some-url.net?${this._objectToUrlParams(reqObj)}`, {
        //   method: "GET",
        // });

        localStorage.setItem("users", JSON.stringify(this._defaultStudents));

        this._initTable();

        this._showToast("Success!");
      }
    });
  };

  _initDeleteStudentModal = () => {
    const deleteModal = document.querySelector(".delete-user-modal");
    const deleteForm = deleteModal.querySelector("#delete-student-form");

    deleteForm.addEventListener("submit", (e) => {
      e.preventDefault();

      if (this._formView === "delete") {
        this._defaultStudents = this._defaultStudents.filter(
          (student) => student.id !== this._activeStudentId
        );

        // request
        const reqObj = {
          id: this._activeStudentId,
        };
        fetch(`http://some-url.net?${this._objectToUrlParams(reqObj)}`, {
          method: "GET",
        });

        localStorage.setItem("users", JSON.stringify(this._defaultStudents));

        this._initTable();
      }
    });
  };

  _createTableRow = (user) => {
    const handleEditClick = () => {
      this._openEditUserModal(user.id);
      this._activeStudentId = user.id;
    };

    const handleDeleteClick = () => {
      this._openDeleteUserModal(user.id);
      this._activeStudentId = user.id;
    };

    const handleSelect = (e) => {
      const isChecked = e.currentTarget.checked;
      const statusIndicator =
        e.currentTarget.parentNode.parentNode.querySelector(".indicator-container");

      if (isChecked) {
        statusIndicator.classList.add("active");
      } else {
        statusIndicator.classList.remove("active");
      }
    };

    const rowTemplate = `
    <tr>
        <td><input ${user.isActive && "checked"} type="checkbox"/></td>
        <td>${user.group}</td>
        <td>${user.firstName} ${user.lastName}</td>
        <td>${user.gender}</td>
        <td>${user.birthday}</td>
        <td><div class="indicator-container ${
          user.isActive ? " active" : ""
        }"><div class="indicator"></div></div></td>
        <td>
            <div class="options-buttons-container">
                <button class="options-button edit">
                    <img src="./images/pencil.svg"/>
                </button>
                <button class="options-button delete">X</button>
            </div>
        </td>
    </tr>
  `;

    this._tableBody.insertAdjacentHTML("beforeend", rowTemplate);

    const lastStudent = this._tableBody.querySelector("tr:last-child");
    const editButton = lastStudent.querySelector(".edit");
    const deleteButton = lastStudent.querySelector(".delete");
    const statusCheckbox = lastStudent.querySelector("input[type='checkbox']");

    editButton.addEventListener("click", handleEditClick);
    deleteButton.addEventListener("click", handleDeleteClick);
    statusCheckbox.addEventListener("change", handleSelect);
  };

  _initSelectAllCheckbox = () => {
    const selectAllCheckbox = document.querySelector(
      ".students-table .table-checkbox-container.main input"
    );

    selectAllCheckbox.addEventListener("change", (e) => {
      const rowsStatusCheckboxes = document.querySelectorAll(".students-table tbody tr input");

      rowsStatusCheckboxes.forEach((checkbox) => {
        let event = new Event("change");
        checkbox.checked = e.currentTarget.checked;
        checkbox.dispatchEvent(event);
      });
      this._defaultStudents.forEach((student) => {
        student.isActive = e.currentTarget.checked;
      });
    });
  };

  _openAddUserModal = () => {
    const addUserModal = document.querySelector(".edit-user-modal");
    const addUserModalTitle = addUserModal.querySelector(".title h2");
    const addUserForm = addUserModal.querySelector("#edit-student-form");

    addUserModal.classList.add("active");
    addUserModalTitle.textContent = "Add student";
    addUserForm.reset();
    this._formView = "add";
    this._activeStudentId =
      this._defaultStudents.length > 0
        ? this._defaultStudents[this._defaultStudents.length - 1].id + 1
        : 0;

    console.log(
      "asdad",
      this._defaultStudents.length > 0
        ? this._defaultStudents[this._defaultStudents.length - 1].id + 1
        : 0
    );
  };

  _openEditUserModal = (studentId) => {
    const student = this._defaultStudents.find((student) => student.id === studentId);

    const editUserModal = document.querySelector(".edit-user-modal");
    const editUserModalTitle = editUserModal.querySelector(".title h2");
    const editUserForm = editUserModal.querySelector("#edit-student-form");
    const userIdInput = editUserForm.querySelector("#student-id");

    const groupInput = editUserModal.querySelector("#group");
    const firstNameInput = editUserModal.querySelector("#first-name");
    const lastNameInput = editUserModal.querySelector("#last-name");
    const genderInput = editUserModal.querySelector("#gender");
    const birthdayInput = editUserModal.querySelector("#birthday");

    editUserModal.classList.add("active");
    editUserModalTitle.textContent = "Edit student";
    editUserForm.reset();
    userIdInput.value = studentId ?? "";
    genderInput.value = student.gender ?? "";
    groupInput.value = student.group ?? "";
    firstNameInput.value = student.firstName ?? "";
    lastNameInput.value = student.lastName ?? "";
    birthdayInput.value = student.birthday ?? "";
    this._formView = "edit";
  };

  _openDeleteUserModal = (studentId) => {
    const deleteUserModal = document.querySelector(".delete-user-modal");
    const studentIdInput = deleteUserModal.querySelector("#delete-student-id");

    deleteUserModal.classList.add("active");
    this._formView = "delete";
    this._activeStudentId = studentId;
    studentIdInput.value = studentId;
  };
}

document.addEventListener("DOMContentLoaded", () => {
  const app = new StudentsApp();
  app.run();
});
